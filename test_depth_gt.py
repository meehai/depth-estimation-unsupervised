import torch as tr
import numpy as np
from Mihlib import plot_image, show_plots, saveImage, npGetInfo, figure_set_size
import matplotlib.pyplot as plt
from neural_wrappers.utilities import minMaxNormalizeData
from neural_wrappers.pytorch import getTrData

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

@tr.no_grad()
def test_depth_gt(model, generator, save, plot):
	print(save, plot)
	i = 0
	allDiff = 0
	for _, labels in generator:
		npImg = labels[0]
		npGtDisp = labels[2]
		trImg = tr.from_numpy(npImg).to(device)
		trDisp = model.disp_net(trImg)[0]
		npDisp = trDisp.detach().cpu().numpy()

		# print(npGetInfo(npDisp))
		# print(tgtImage.shape)
		diffs = {"export" : 0, "l1" : 0}
		for j in range(npImg.shape[0]):
			if i == 100:
				break
			i += 1
			refDisp = npGtDisp[j]
			resDisp = npDisp[j]
			diffs["l1"] += np.mean(np.abs(refDisp - resDisp))

			refDisp = minMaxNormalizeData(refDisp, np.min(refDisp), np.max(refDisp))
			resDisp = minMaxNormalizeData(resDisp, np.min(resDisp), np.max(resDisp))
			print(npGetInfo(refDisp))
			print(npGetInfo(resDisp))

			diff = np.sum(np.abs(refDisp - resDisp))
			diffs["export"] += diff
			print("Iter %d. L1 loss: %2.5f, Export Loss: %2.5f" % (i, diffs["l1"] / i, diffs["export"] / i))

			if plot or save:
				plot_image(npImg[j], axis=(1, 3, 1), new_figure=True)
				plot_image(refDisp, cmap="hot", axis=(1, 3, 2), new_figure=False)
				plot_image(resDisp, cmap="hot", axis=(1, 3, 3), new_figure=False, title="Diff: %2.2f" % (diff))
				figure_set_size((12, 8))
			if plot:
				show_plots()
			if save:
				plt.savefig("%d.png" % (i))

		if i == 100:
			break
	allDiff /= i
	print("Diff: %2.5f" % (allDiff))