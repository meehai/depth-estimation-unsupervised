import numpy as np
import math

def isclose(x, y, rtol=1.e-5, atol=1.e-8):
	return abs(x-y) <= atol + rtol * abs(y)

def euler_angles_from_rotation_matrix_zyx(R):
	'''
	From a paper by Gregory G. Slabaugh (undated),
	"Computing Euler angles from a rotation matrix
	'''
	phi = 0.0
	if isclose(R[2,0],-1.0):
		theta = math.pi/2.0
		psi = math.atan2(R[0,1],R[0,2])
	elif isclose(R[2,0],1.0):
		theta = -math.pi/2.0
		psi = math.atan2(-R[0,1],-R[0,2])
	else:
		theta = -math.asin(R[2,0])
		cos_theta = math.cos(theta)
		psi = math.atan2(R[2,1]/cos_theta, R[2,2]/cos_theta)
		phi = math.atan2(R[1,0]/cos_theta, R[0,0]/cos_theta)
	return phi, theta, psi

def euler_angles_from_rotation_matrix_xyz(R):
	phi = 0.0
	if isclose(R[0, 2],1.0):
		theta = math.pi / 2.0
		psi = -math.atan2(R[2,1],R[2,0])
	elif isclose(R[0,2],-1.0):
		theta = -math.pi / 2.0
		psi = math.atan2(R[2,1],R[2,0])
	else:
		theta = math.asin(R[0,2])
		cos_theta = math.cos(theta)
		psi = -math.atan2(R[0,1]/cos_theta, R[0,0]/cos_theta)
		phi = -math.atan2(R[1,2]/cos_theta, R[2,2]/cos_theta)
	return phi, theta, psi

def euler_angles_from_rotation_matrix(R, seq="zyx"):
	if seq == "zyx":
		return euler_angles_from_rotation_matrix_zyx(R)
	else:
		return euler_angles_from_rotation_matrix_xyz(R)

def fRT(R, t):
	return np.vstack((np.hstack((R,np.array(t).reshape((3, 1)))), [0, 0, 0, 1]))

def findInverseT(R, t):
	return np.linalg.inv(fRT(R, t))[0:-1,3]

def rotx(a):
	return np.array([[1, 0, 0],[0, np.cos(a), -np.sin(a)], [0, np.sin(a), np.cos(a)]])

def roty(a):
	return np.array([[np.cos(a), 0, np.sin(a)], [0, 1, 0], [-np.sin(a), 0, np.cos(a)]])

def rotz(a):
	return np.array([[np.cos(a), -np.sin(a), 0], [np.sin(a), np.cos(a), 0], [0, 0, 1]])

def eul2Rot(eul, seq="zyx"):
	seqFcts = {"x" : rotx, "y" : roty, "z": rotz}
	res = seqFcts[seq[0]](eul[0])
	for i in range(1, len(seq)):
		res = res @ seqFcts[seq[i]](eul[i])
	return res

def eul2RotFlipped(eul, seq):
	return np.flip(np.flip(eul2Rot(eul, seq).T, axis=0), axis=1)

def getInvRInvT(R, t, seq="zyx"):
	t_inv = findInverseT(R, t)
	eul_R_inv = euler_angles_from_rotation_matrix(np.linalg.inv(R), seq)
	return eul_R_inv, t_inv