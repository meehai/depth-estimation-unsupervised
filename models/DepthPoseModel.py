import torch as tr
import torch.nn as nn
import torch.backends.cudnn as cudnn
from .DispNetS import DispNetS
from .PoseExpNet import PoseExpNet

from neural_wrappers.pytorch import NeuralNetworkPyTorch

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

class DepthPoseModel(NeuralNetworkPyTorch):
	def __init__(self, **kwargs):
		super().__init__(**kwargs)

		disp_net = DispNetS().to(device)
		pose_exp_net = PoseExpNet(output_exp=self.hyperParameters["explainabilityMask"]).to(device)
		pose_exp_net.init_weights()
		disp_net.init_weights()

		cudnn.benchmark = True
		self.disp_net = nn.DataParallel(disp_net)
		self.pose_exp_net = nn.DataParallel(pose_exp_net)

	def forward(self, x):
		pass

	def computePoses(self, tgtRgb, refRgbs):
		# Pose network receives input one by one (img1, img2), so we have to pass all the images in the sequence
		MB = refRgbs.shape[0]
		seqSize = refRgbs.shape[1]

		poses = tr.zeros(MB, seqSize, 6).to(device)
		thisPoseNetResult = self.pose_exp_net(tgtRgb, refRgbs[:, 0])
		poses[:, 0] = thisPoseNetResult["pose"]

		if self.hyperParameters["explainabilityMask"]:
			masks = [tr.zeros(MB, seqSize, *x.shape[-2 : ]).to(device) for x in thisPoseNetResult["explainabilityMask"]]
			for j, mask in enumerate(thisPoseNetResult["explainabilityMask"]):
				masks[j][:, 0] = mask

		for i in range(1, seqSize):
			thisPoseNetResult = self.pose_exp_net(tgtRgb, refRgbs[:, i])
			poses[:, i] = thisPoseNetResult["pose"]

			# If we output explainability mask (at all scales), concatenate the result for all the images in the
			#  sequence. Otherwsie, do nothing.
			if self.hyperParameters["explainabilityMask"]:
				for j, mask in enumerate(thisPoseNetResult["explainabilityMask"]):
					masks[j][:, i] = mask

		poseNetResult = {"pose" : poses}
		if self.hyperParameters["explainabilityMask"]:
			poseNetResult["explainabilityMask"] = masks
		return poseNetResult

	def computeReversePoses(self, tgtRgb, refRgbs):
		MB = refRgbs.shape[0]
		seqSize = refRgbs.shape[1]
		poses = tr.zeros(MB, seqSize, 6).to(device)
		for i in range(seqSize):
			poses[:, i] = self.pose_exp_net(refRgbs[:, i], tgtRgb, output_exp=False)["pose"]
		return {"pose" : poses}

	def networkAlgorithm(self, trInputs, trLabels):
		tgtRgb, refRgbs = trInputs

		disparities = self.disp_net(tgtRgb)
		poseNetResult = self.computePoses(tgtRgb, refRgbs)

		trResults = {"disparities" : disparities, "pose" : poseNetResult["pose"]}
		if self.hyperParameters["explainabilityMask"]:
			trResults["explainabilityMask"] = poseNetResult["explainabilityMask"]
		if self.hyperParameters["reversePose"] > 0:
			reversePoseNetResult = self.computeReversePoses(tgtRgb, refRgbs)
			trResults["reversePose"] = reversePoseNetResult["pose"]
		if self.hyperParameters["identityPose"] > 0:
			trResults["identityPose"] = self.pose_exp_net(tgtRgb, tgtRgb, output_exp=False)["pose"]

		trLoss = self.criterion(trResults, trLabels)
		return trResults, trLoss