import torch as tr
import numpy as np
import pims
from path import Path
from neural_wrappers.utilities import resize_batch
from matplotlib.cm import inferno
from Mihlib import plot_image, show_plots, saveImage, npGetInfo

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

def minMaxNormalize(x):
	x = np.float32(x)
	Min, Max = np.min(x), np.max(x)
	return (x - Min) / (Max - Min)

def test_depth_video(model, video_path, startIndex, saveImages, plotImages, batchSize):
	if not saveImages is None:
		output_path = Path(saveImages)
		output_path.makedirs_p()
	video = pims.Video(video_path)

	N = len(video) - (len(video) // 100)
	for i in range(startIndex, N, batchSize):
		endIndex = min((i + batchSize), N)
		print("[%d:%d]/%d" % (i, endIndex, N))
		frame = resize_batch(np.float32(video[i : endIndex]), (480, 854, 3)) / 255
		npImg = np.float32((frame - 0.5) / 0.5)
		npImg = np.transpose(npImg, (0, 3, 1, 2))

		with tr.no_grad():
			trImg = tr.from_numpy(npImg).to(device)
			trDisp = model.disp_net(trImg)[0]
			print(trDisp.min(), trDisp.max(), trDisp.mean(), trDisp.std())
			npDisp = trDisp.detach().cpu().numpy()
			npDepth = 1 / npDisp

		for j in range(endIndex - i):
			thisNpDisp = minMaxNormalize(npDisp[j])
			thisOrigImg = frame[j]
			if plotImages:
				plot_image(thisOrigImg, axis=(1, 2, 1), new_figure=True, title="Img1")
				plot_image(thisNpDisp, axis=(1, 2, 2), new_figure=False, title="Disparity", cmap="inferno")
				show_plots()

			if not saveImages is None:
				thisNpDisp = inferno(thisNpDisp)[..., 0 : 3]
				bothOut = np.zeros((thisOrigImg.shape[0], 10 + 2 * thisOrigImg.shape[1], 3), dtype=np.uint8)
				bothOut[:, 0 : thisOrigImg.shape[1]] = thisOrigImg * 255
				bothOut[:, 10 + thisOrigImg.shape[1] : ] = thisNpDisp * 255
				saveImage(bothOut, "%s/%d.png" % (output_path, i + j))
				np.save("%s/%d.npy" % (output_path, i + j), npDisp[j])

if __name__ == '__main__':
	main()
