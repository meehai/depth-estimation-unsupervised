import h5py
from neural_wrappers.readers import DatasetReader
from neural_wrappers.utilities import resize_batch, minMaxNormalizeData
import numpy as np
import random
from Mihlib import npGetInfo
from functools import partial

def min_max_paper(data, dim, obj):
	min = obj.minimums[dim]
	max = obj.maximums[dim]
	return (minMaxNormalizeData(data, min, max) - 0.5) / 0.5

class VideoDepthReader(DatasetReader):
	def __init__(self, datasetPath, seqSize, allDims=["rgb", "intrinsics"], dataDims=["rgb", "intrinsics"], \
		labelDims=[], dimTransform={"rgb" : np.float32, "intrinsics" : np.float32}, \
		dataFinalTransform = lambda x : x, labelFinalTransform = lambda x : x, **kwargs):
		if "normalizer" in kwargs:
			normalizer=kwargs["normalizer"]
		else:
			normalizer={"rgb" : ("min_max_paper", partial(min_max_paper, obj=self))}
		super().__init__(datasetPath, allDims=allDims, dataDims=dataDims, labelDims=labelDims, normalizer=normalizer, \
			dimTransform=dimTransform, dataFinalTransform=dataFinalTransform, labelFinalTransform=labelFinalTransform)

		self.dataset = h5py.File(self.datasetPath, "r")
		self.numData = {item : len(self.dataset[item]["rgb"]) for item in self.dataset}
		self.seqSize = seqSize
		self.sceneItems = self.dataset["train"]["rgb"].shape[1]
		assert seqSize >= 2 and seqSize <= self.sceneItems
		self.seqTgtIndices, self.seqRefIndices = self.computeRefTgtIndices()

		self.maximums = {
			"rgb" : np.array([255, 255, 255]),
		}

		self.minimums = {
			"rgb" : np.array([0, 0, 0]),
		}
		print("[VideoDepthReader] Setup complete.")

	def computeRefTgtIndices(self):
		ix = np.arange(self.sceneItems)
		seqTgtIndices = np.zeros((self.sceneItems - self.seqSize + 1,), dtype=np.uint8)
		seqRefIndices = np.zeros((self.sceneItems - self.seqSize + 1, self.seqSize - 1), dtype=np.uint8)
		for i in range(self.sceneItems - self.seqSize + 1):
			refIndices = ix[i : i + self.seqSize]
			tgtIndices = refIndices[self.seqSize // 2]
			res = np.delete(refIndices, self.seqSize // 2)
			# print(res.shape, refIndices.shape)
			seqRefIndices[i] = np.delete(refIndices, self.seqSize // 2)
			seqTgtIndices[i] = tgtIndices

		return seqTgtIndices, seqRefIndices

	def getNumIterations(self, type, miniBatchSize, accountTransforms=True):
		N = super().getNumIterations(type, miniBatchSize, accountTransforms)
		N *= (self.sceneItems - self.seqSize + 1)
		return N

	def iterateSeq(self, type, data, labels):
		N = self.sceneItems - self.seqSize + 1
		rgb, intrinsics = data
		intrinsics_inv = np.linalg.inv(intrinsics)

		for i in range(N):
			refRgbs = rgb[:, self.seqRefIndices[i]].swapaxes(2, 4).swapaxes(3, 4)
			tgtRgb = rgb[:, self.seqTgtIndices[i]].swapaxes(1, 3).swapaxes(2, 3)
			shape = tgtRgb.shape
			tgtDisp = np.float32(np.zeros((shape[0], shape[2], shape[3])))
			yield (tgtRgb, refRgbs), (tgtRgb, refRgbs, tgtDisp, intrinsics, intrinsics_inv)

	def iterate_once(self, type, miniBatchSize):
		dataset = self.dataset[type]
		numIterations = super().getNumIterations(type, miniBatchSize, accountTransforms=False)

		for i in range(numIterations):
			startIndex = i * miniBatchSize
			endIndex = min((i + 1) * miniBatchSize, self.numData[type])
			assert startIndex < endIndex, "startIndex < endIndex. Got values: %d %d" % (startIndex, endIndex)
			numData = endIndex - startIndex

			for items in self.getData(dataset, startIndex, endIndex):
				data, labels = items
				for seqData, seqLabels in self.iterateSeq(type, data, labels):
					yield seqData, seqLabels

class CityScapesSequenceReader(VideoDepthReader):
	def __init__(self, datasetPath, seqSize):
		normalization = ("min_max_paper", partial(min_max_paper, obj=self))
		super().__init__(datasetPath, seqSize=seqSize, allDims=["rgb", "disparity", "intrinsics"], \
			dataDims=["rgb", "intrinsics"], labelDims=["disparity"], \
			dimTransform={"rgb" : np.float32, "disparity" : np.float32, "intrinsics" : np.float32}, \
			normalizer={"rgb" : normalization, "disparity" : "min_max_normalization"}, \
			dataFinalTransform=lambda x : x, labelFinalTransform=lambda x : np.concatenate(x, axis=-1))

		self.maximums["disparity"] = np.array([32767])
		self.minimums["disparity"] = np.array([0])
		print("[DepthValReader] Setup complete.")

	def iterateSeq(self, type, data, labels):
		N = self.sceneItems - self.seqSize + 1
		rgb, intrinsics = data
		intrinsics_inv = np.linalg.inv(intrinsics)

		for i in range(N):
			refRgbs = rgb[:, self.seqRefIndices[i]].swapaxes(2, 4).swapaxes(3, 4)
			tgtRgb = rgb[:, self.seqTgtIndices[i]].swapaxes(1, 3).swapaxes(2, 3)
			tgtDisp = labels[:, self.seqTgtIndices[i]]
			yield (tgtRgb, refRgbs), (tgtRgb, refRgbs, tgtDisp, intrinsics, intrinsics_inv)