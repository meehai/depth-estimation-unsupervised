from Mihlib import npGetInfo
import pims
import sys
import numpy as np
import h5py
sys.path.append("/Date1/hpc/optical-flow/pytorch-pwc/")
from inference import runImages
from neural_wrappers.utilities import resize_batch
from argparse import ArgumentParser
from tqdm import tqdm

def getArgs():
	parser = ArgumentParser()

	parser.add_argument("videos_list")
	parser.add_argument("out_file", default="out.h5")
	parser.add_argument("--intrinsic_list")
	parser.add_argument("--resolution", default="480,854")
	parser.add_argument("--sequence_size", type=int, default=15)
	parser.add_argument("--train_val_split", default=0.8)
	parser.add_argument("--num_tries_per_video", type=int, default=2, help="Number of loops in the same video for " \
		"valid frames.")

	args = parser.parse_args()
	args.resolution = list(map(lambda x : int(x), args.resolution.split(",")))
	return args

def doCurrentSequence(video, markedUsed, seqSize, endIndex, ixCurrentFrame, resolution):
	seqFrames = []
	seqIndices = []

	while markedUsed[ixCurrentFrame] != False:
		ixCurrentFrame += 1
	frame1 = np.array(video[ixCurrentFrame : ixCurrentFrame + 1])

	while True:
		# If video is ended, but sequence still up, exit from the function.
		if ixCurrentFrame >= endIndex:
			return None, None, endIndex

		# Add this frame to the sequence list and mark it as used
		seqFrames.append(frame1)
		markedUsed[ixCurrentFrame] = True
		seqIndices.append(ixCurrentFrame)

		# If this frame marks the end of a sequence, return the list of frames
		if len(seqFrames) == seqSize:
			seqFrames = np.concatenate(seqFrames, axis=0)
			seqFrames = resize_batch(seqFrames, (*resolution, 3))
			return seqFrames, seqIndices, ixCurrentFrame

		ixNextFrame = ixCurrentFrame
		while True:
			ixNextFrame += 1
			if ixNextFrame >= endIndex:
				return None, None, endIndex

			if markedUsed[ixNextFrame] == True:
				continue

			frame2 = np.array(video[ixNextFrame : ixNextFrame + 1])
			u, v = runImages(frame1, frame2)
			mag = (u**2 + v**2)[0]
			mag = np.mean(mag)

			# check that the flow isn't too big (other magic number). If it is, then the sequence is too
			#  dynamic, so we drop it as well and destroy the entire sequence.
			if mag >= 2:
				return None, None, ixNextFrame

			# Maic number from cityscapes statistics (minimum flow w/o outliers in that dataset)
			# Also, this frame must not be used, so we don't overlap other subsequences
			if mag > 0.054:
				break

		ixCurrentFrame = ixNextFrame
		frame1 = frame2

def doCurrentVideo(video, vidIndex, seqSize, resolution, numTriesPerVideo):
	res = []
	N = len(video)
	startIndex = N // 10
	endIndex = (9 * N) // 10
	markedUsed = np.zeros(N, dtype=np.bool)

	for i in range(numTriesPerVideo):
		ixCurrentFrame = 0
		# Start to try with this sequence
		while True:
			frames, seqIndices, ixCurrentFrame = doCurrentSequence(video, markedUsed, \
				seqSize, endIndex, ixCurrentFrame, resolution)
			if ixCurrentFrame >= endIndex:
				break

			if not frames is None:
				res.append(frames)
				print("[Vid: %d. Try: %d/%d. Ix:%d/%d]. Adding new sequence %s. Total: %d" % \
					(vidIndex, i, numTriesPerVideo, ixCurrentFrame, endIndex, seqIndices, len(res)))
	return res

def main():
	args = getArgs()
	videoPaths = list(map(lambda x : x.replace("\n", ""), open(args.videos_list).readlines()))
	print("Exporting %d videos at resolution %s and seq size %d" % \
		(len(videoPaths), args.resolution, args.sequence_size))

	res = []
	for vidIndex, path in enumerate(videoPaths):
		video = pims.Video(path)
		fps = video.frame_rate
		duration = max(0, len(video) // fps)
		print("[%d/%d] In video: %s. FPS: %s. Duration: %s." % (vidIndex + 1, len(videoPaths), \
			videoPaths[vidIndex], fps, duration))
		vidRes = doCurrentVideo(video, vidIndex, args.sequence_size, args.resolution, args.num_tries_per_video)
		res.extend(vidRes)

	res = np.array(res)
	print(res.shape)
	perm = np.random.permutation(len(res))
	res = res[perm]
	file = h5py.File(args.out_file, "w")
	trainCount = int(args.train_val_split * len(res))
	print("Train count %d. Val count %d" % (trainCount, len(res) - trainCount))

	file.create_group("train")
	file["train"]["rgb"] = res[0 : trainCount]
	if args.train_val_split > 0:
		file.create_group("validation")
		file["validation"]["rgb"] = res[trainCount : ]
	# TODO: export intrinsics

	print("Done")

if __name__ == "__main__":
	main()