import torch as tr
from Mihlib import plot_image, show_plots, figure_set_size, saveImage
from inverse_warp import inverse_warp
import matplotlib.pyplot as plt
from neural_wrappers.pytorch import getTrData

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

@tr.no_grad()
def test_warp(model, generator, save, plot):
	i = 0
	for data, labels in generator:
		trData = getTrData(data)
		trLabels = getTrData(labels)
		img1, img2, gtDisp, K, K_inv = trLabels
		print(img1.shape, img2.shape)
		img1 = img1.to(device)
		imgs2 = [x.to(device) for x in img2]
		nRows = len(imgs2)

		result = model.networkAlgorithm(trData, trLabels)[0]
		poses = result["pose"]
		disp = result["disparities"][0][:, 0]
		depth = 1 / disp
		K = K.to(device)
		K_inv = K_inv.to(device)
		poses = poses.transpose(0, 1)

		imgs1_warped = [inverse_warp(imgs2[k], depth, poses[k], K, K_inv, \
			"euler", "zeros") for k in range(len(imgs2))]

		for j in range(img1.shape[0]):
			if save or plot:
				plot_image(img1[j], axis=(2, 3, 1), new_figure=True, title="Img1")
				plot_image(disp[j].detach(), axis=(2, 3, 4), new_figure=False, title="Depth", cmap="inferno")

			for k in range(len(imgs2)):
				E = tr.sum(tr.abs(img1[j] - imgs1_warped[k][j]))
				E_img = tr.abs(img1[j] - imgs1_warped[k][j])
				E_img = E_img.sum(dim=0).detach().cpu().numpy()
				E_img_binary = (E_img < 0.1).astype(int)

				if save or plot:
					plot_image(imgs2[k][j], axis=(nRows, 6, 6 * k + 3), new_figure=False, title="Img2")
					plot_image(imgs1_warped[k][j].detach(), axis=(nRows, 6, 6 * k + 4), new_figure=False,
						title="Img2->Img1 Error: %2.2f" % (E))
					plot_image(E_img, axis=(nRows, 6, 6 * k + 5), new_figure=False, title="Diff image")
					plot_image(E_img_binary, axis=(nRows, 6, 6 * k + 6), new_figure=False, \
						title="Mask good pixels")
			if save or plot:
				figure_set_size((20, 8))
			if save:
				plt.savefig("%d.png" % (i))
			if plot:
				show_plots()

			i += 1
			if i == 100:
				break