import os
import torch as tr
import torch.optim
import torch.nn as nn
import torch.utils.data
from path import Path
from argparse import ArgumentParser
from functools import partial
from neural_wrappers.callbacks import SaveModels, SaveHistory, PlotMetricsCallback, Callback
from datetime import datetime

from loss_functions import lossFn, photometric_reconstruction_metric, l1_metric, l1_export_metric
from sequence_folders import SequenceFolder
from models import DepthPoseModel
from test_warp import test_warp
from test_depth_video import test_depth_video
from test_depth_gt import test_depth_gt
from test_go_crazy import test_go_crazy

def getArgs():
	parser = ArgumentParser()

	parser.add_argument("type")
	parser.add_argument("data", help="path to dataset")

	parser.add_argument("--dataset", default="video_sequence")
	parser.add_argument("--validation_data", help="path to validation dataset. If not set, assumes same as data")
	parser.add_argument("--sequence_length", type=int, help="sequence length for training", default=3)
	parser.add_argument("--epochs", default=200, type=int, help="number of total epochs to run")
	parser.add_argument("--batch_size", default=4, type=int, help="mini-batch size")
	parser.add_argument("--lr", "--learning-rate", default=2e-4, type=float, help="initial learning rate")
	parser.add_argument("--weights_file")

	# Training options
	parser.add_argument("--photo_loss_weight", type=float, help="weight for photometric loss", default=1)
	parser.add_argument("--mask_loss_weight", type=float, help="weight for explainability mask loss", default=1)
	parser.add_argument("--smooth_loss_weight", type=float, help="weight for disparity smoothness loss", default=2)
	parser.add_argument("--ground_truth_loss_weight", type=float, default=0)
	parser.add_argument("--ground_truth_l2_loss_weight", type=float, default=0)
	parser.add_argument("--reverse_pose_constraint_weight", type=float, default=0)
	parser.add_argument("--identity_pose_constraint_weight", type=float, default=0)

	# Testing options
	parser.add_argument("--test_plot", type=int, default=0)
	parser.add_argument("--test_save")
	parser.add_argument("--test_video_start_index", type=int, default=0)
	parser.add_argument("--test_video_resolution") # 480,854 or 128,416

	args = parser.parse_args()
	if args.type == "retrain":
		assert not args.weights_file is None
		args.dir = os.path.dirname(args.weights_file)
	else:
		cwd = os.path.abspath(os.path.dirname(__file__))
		now = str(datetime.now()).replace(" ", "-")
		now = now[0 : now.find(".")]
		args.dir = Path("%s/checkpoints/seq%d-p%2.2f-m%2.2f-s%2.2f-gt%2.2f-rp%2.2f-ip%2.2f/%s" % \
			(cwd, args.sequence_length, \
			args.photo_loss_weight, args.mask_loss_weight, args.smooth_loss_weight, \
			args.ground_truth_loss_weight, args.reverse_pose_constraint_weight, \
			args.identity_pose_constraint_weight, \
			now))
	print("Working directory %s" % (args.dir))

	if args.type == "train":
		args.dir.makedirs_p()
	if args.validation_data is None:
		args.validation_data = args.data
	args.test_plot = bool(args.test_plot)
	assert args.dataset in ("cityscapes_sequence", "video_sequence")

	if args.type == "test_depth_video":
		args.test_video_resolution = list(map(lambda x : float(x), args.test_video_resolution.split(",")))
		assert len(args.test_video_resolution) == 2

	if args.type == "test_go_crazy":
		args.test_video_resolution = list(map(lambda x : float(x), args.test_video_resolution.split(",")))
		assert len(args.test_video_resolution) == 2
	return args

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

def main():
	args = getArgs()

	# create model
	print("=> creating model")
	tr.manual_seed(0)
	model = DepthPoseModel(hyperParameters={"sequenceLength" : args.sequence_length, \
		"explainabilityMask" : args.mask_loss_weight > 0, "reversePose" : args.reverse_pose_constraint_weight, \
		"identityPose" : args.identity_pose_constraint_weight})
	model = model.to(device)
	print(model.summary())

	if args.type not in ["test_depth_video", "test_go_crazy"]:
		if args.dataset == "cityscapes_sequence":
			from reader import CityScapesSequenceReader as Reader
		elif args.dataset == "video_sequence":
			from reader import VideoDepthReader as Reader
			assert args.ground_truth_loss_weight == 0

		reader = Reader(args.data, args.sequence_length)
		generator = reader.iterate("train", args.batch_size)
		numSteps = reader.getNumIterations("train", args.batch_size)
		valReader = Reader(args.validation_data, args.sequence_length)
		valGenerator = valReader.iterate("validation", args.batch_size)
		valNumSteps = valReader.getNumIterations("validation", args.batch_size)

	weights = {"photometric" : args.photo_loss_weight, "explainability" : args.mask_loss_weight, \
		"smooth" : args.smooth_loss_weight, "l1" : args.ground_truth_loss_weight, \
		"reversePose" : args.reverse_pose_constraint_weight, "identityPose" : args.identity_pose_constraint_weight}
	print("Weights:", str(weights)[1:-1].replace("'", "\"").replace(": " , "=>"))
	model.setCriterion(partial(lossFn, weights=weights))

	metrics = {}
	if args.dataset == "cityscapes_sequence":
		metrics["l1_metric"] = l1_metric
		metrics["l1_export_metric"] = l1_export_metric
	# Only set tihs metric and callback if we optimize it, otherwise will NaN and an assert will fail.
	if weights["photometric"] > 0:
		metrics["photometric"] = photometric_reconstruction_metric
	model.setMetrics(metrics)

	if args.type == "train":
		print("=> setting adam solver")
		model.setOptimizer(tr.optim.Adam, lr=args.lr)

		callbacks = [SaveHistory("history.txt")]
		plotMetrics, plotDirections = [], []

		# Only cityscapes sequence dataset has disparity ground truth.
		if args.dataset == "cityscapes_sequence":
			plotMetrics.extend(["Loss", "l1_metric"])
			plotDirections.extend(["min", "min"])
			callbacks.append(SaveModels("best", metric="l1_metric"))

		if weights["photometric"] > 0:
			callbacks.append(SaveModels("best", metric="photometric"))
			plotMetrics.append("photometric")
			plotDirections.append("min")
		if len(plotMetrics) > 0:
			callbacks.append(PlotMetricsCallback(plotMetrics, plotDirections))

		os.chdir(args.dir)
		model.train()
		model.train_generator(generator, numSteps, numEpochs=args.epochs, callbacks=callbacks,
			validationGenerator=valGenerator, validationSteps=valNumSteps)

	elif args.type == "retrain":
		model.loadModel(args.weights_file)
		os.chdir(args.dir)
		model.train_generator(generator, numSteps, numEpochs=args.epochs, callbacks=None,
			validationGenerator=valGenerator, validationSteps=valNumSteps)

	elif args.type == "test_warp":
		model.loadWeights(args.weights_file)
		test_warp(model, valGenerator, args.test_save, args.test_plot)

	elif args.type == "test_depth_video":
		model.loadWeights(args.weights_file)
		test_depth_video(model, args.data, args.test_video_start_index, \
			args.test_save, args.test_plot, args.batch_size)

	elif args.type == "test_go_crazy":
		model.loadWeights(args.weights_file)
		model.eval()
		test_go_crazy(model, args.data, args.test_video_start_index)

	elif args.type == "test_depth_gt":
		model.loadWeights(args.weights_file)
		test_depth_gt(model, valGenerator, args.test_save, args.test_plot)

	elif args.type == "test":
		model.loadWeights(args.weights_file)
		results = model.test_generator(valGenerator, 10, callbacks=[MyCallback()])
		print(results)

if __name__ == "__main__":
	main()
