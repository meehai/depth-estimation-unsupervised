from __future__ import division
import numpy as np
import torch as tr
from torch import nn
import torch.nn.functional as F
from torch.autograd import Variable
from inverse_warp import inverse_warp
from neural_wrappers.utilities import minMaxNormalizeData
from neural_wrappers.pytorch import getNpData, getTrData
from pose_utils import getInvRInvT, eul2Rot

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")
currentLoss = None

# @param[in] y_pred A dictionary returned by DepthPoseModel, which has keys various results. Minimum required keys are
#  "disparities" and "pose". Optional keys are:
#   - "explainabilityMask" if explainability loss is enabled.
#   - "reversePose" if reverse pose loss is enabled.
# @param[in] y_true The ground truth that is computed by the reader (can contain gt pose (TODO) or gt depth/disparity
#  depending on the source (and the weights of the losses)
# @param[in] weights The weights of the losses. All of them are combined using a weighted sum, based on these values.
# @return The loss that is used to train the network.
def lossFn(y_pred, y_true, weights):
	tgt_img, ref_imgs, tgt_disp, intrinsics, intrinsics_inv = y_true
	depth = [1 / disp for disp in y_pred["disparities"]]
	global currentLoss

	losses = {k : 0 for k in weights.keys()}
	if weights["photometric"] > 0:
		explainability_mask = None if not "explainabilityMask" in y_pred else y_pred["explainabilityMask"]
		losses["photometric"] = photometric_reconstruction_loss(tgt_img, ref_imgs, intrinsics, intrinsics_inv, \
			depth, explainability_mask, y_pred["pose"], "euler", "zeros")
	if weights["explainability"] > 0:
		losses["explainability"] = explainability_loss(y_pred["explainabilityMask"])
	if weights["smooth"] > 0:
		losses["smooth"] = smooth_loss(depth)
	if weights["l1"] > 0:
		losses["l1"] = l1_loss(y_pred, y_true)
	if weights["reversePose"] > 0:
		losses["reversePose"] = reverse_pose_loss(y_pred, y_true)
	if weights["identityPose"] > 0:
		# This is supposed to be 0 by default (pose(im, im) = 0), so sum must also be 0
		losses["identityPose"] = y_pred["identityPose"].mean()

	loss = 0
	for key in weights.keys():
		loss = loss + weights[key] * losses[key]
	currentLoss = losses
	return loss

def l1_metric(y_pred, y_true, **kwargs):
	global currentLoss
	return getNpData(currentLoss["l1"])

def l1_export_metric(y_pred, y_true, **kwargs):
	with tr.no_grad():
		resDisp = y_pred["disparities"]
		tgt_disp, ref_imgs, refDisp, intrinsics, intrinsics_inv = y_true

		resDisp = resDisp[0]
		resDisp = minMaxNormalizeData(resDisp, np.min(resDisp, axis=(1, 2), keepdims=True), \
			np.max(resDisp, axis=(1, 2), keepdims=True))
		refDisp = minMaxNormalizeData(refDisp, np.min(refDisp, axis=(1, 2), keepdims=True), \
			np.max(refDisp, axis=(1, 2), keepdims=True))

		diff = np.abs(refDisp - resDisp)
		diff = np.sum(diff, axis=(1, 2))
	return np.mean(diff)

def photometric_reconstruction_metric(y_pred, y_true, **kwargs):
	global currentLoss
	return getNpData(currentLoss["photometric"])

def reverse_pose_loss(y_pred, y_true):
	pose = y_pred["pose"]
	npPose = getNpData(pose)
	reverse_pose = getNpData(pose) * 0

	for i in range(len(pose)):
		for j in range(len(pose[0])):
			R, t = eul2Rot(npPose[i, j, 3 : ], seq="xyz"), npPose[i, j, 0 : 3]
			eul_R_inv, t_inv = getInvRInvT(R, t, seq="xyz")
			reverse_pose[i, j, 0 : 3] = t_inv
			reverse_pose[i, j, 3 : ] = eul_R_inv
	reverse_pose = getTrData(reverse_pose).requires_grad_(False)
	return tr.mean(tr.abs(y_pred["reversePose"] - reverse_pose))

def l1_loss(y_pred, y_true):
	resDisp = y_pred["disparities"]
	tgt_disp, ref_imgs, refDisp, intrinsics, intrinsics_inv = y_true
	refDisp = refDisp.unsqueeze(1)
	refDispScaled = [F.interpolate(refDisp, (x.shape[-2], x.shape[-1]), mode="area") for x in resDisp]

	diff = [tr.mean(tr.sum(tr.abs(x - y), dim=(1,2,3))) for x, y in zip(refDispScaled, resDisp)]
	diff = sum(diff) / len(diff)
	return diff

def photometric_reconstruction_loss(tgt_img, ref_imgs, intrinsics, intrinsics_inv, depth, explainability_mask, \
	pose, rotation_mode='euler', padding_mode='zeros'):
	# print("rgb_img:", tgt_img.shape)
	# print("ref_imgs:", ref_imgs.shape)
	# print("depths:", [x.shape for x in depth])
	# print("poses:", pose.shape)
	# if explainability_mask:
		# print("explainability_mask:", [x.shape for x in explainability_mask])
	assert(pose.size(0) == ref_imgs.size(0))
	assert(pose.size(1) == ref_imgs.size(1))


	def isNan(x):
		return (x != x).sum()

	def one_scale(depth, explainability_mask):
		assert(explainability_mask is None or depth.size()[-2 : ] == explainability_mask.size()[-2 : ])

		reconstruction_loss = 0
		b, h, w = depth.size()
		downscale = tgt_img.size(2) / h

		tgt_img_scaled = F.interpolate(tgt_img, (h, w), mode='area')
		ref_imgs_scaled = F.interpolate(ref_imgs, (3, h, w), mode='area')
		# print("tgt_img_scaled:", tgt_img_scaled.shape)
		# print("ref_imgs_scaled:", ref_imgs_scaled.shape)
		intrinsics_scaled = tr.cat((intrinsics[:, 0:2]/downscale, intrinsics[:, 2:]), dim=1)
		intrinsics_scaled_inv = tr.cat((intrinsics_inv[:, :, 0:2]*downscale, intrinsics_inv[:, :, 2:]), dim=2)

		for i in range(ref_imgs_scaled.shape[1]):
			ref_img = ref_imgs_scaled[:, i]
			current_pose = pose[:, i]
			# print("ref_img:", ref_img.shape)
			# print("current_pose:", current_pose.shape)

			ref_img_warped = inverse_warp(ref_img, depth, current_pose, intrinsics_scaled, \
				intrinsics_scaled_inv, rotation_mode, padding_mode)
			# print("ref_img_warped:", ref_img_warped.shape)
			out_of_bound = 1 - (ref_img_warped == 0).prod(1, keepdim=True).type_as(ref_img_warped)
			ref_img_warped[ref_img_warped != ref_img_warped] = 0

			diff = (tgt_img_scaled - ref_img_warped)
			# print("diff:", diff.shape)
			diff = diff * out_of_bound
			if explainability_mask is not None:
				diff = diff * explainability_mask[:,i:i+1].expand_as(diff)

			reconstruction_loss += diff.abs().mean()
			assert((reconstruction_loss == reconstruction_loss).item() == 1)

		return reconstruction_loss

	if type(explainability_mask) not in [tuple, list]:
		explainability_mask = [explainability_mask]
	if type(depth) not in [list, tuple]:
		depth = [depth]

	loss = 0
	for d, mask in zip(depth, explainability_mask):
		loss += one_scale(d, mask)
	return loss

def explainability_loss(mask):
	if type(mask) not in [tuple, list]:
		mask = [mask]
	loss = 0
	for mask_scaled in mask:
		ones_var = Variable(tr.ones(1)).expand_as(mask_scaled).type_as(mask_scaled)
		loss += nn.functional.binary_cross_entropy(mask_scaled, ones_var)
	return loss

def smooth_loss(pred_map):
	def gradient(pred):
		D_dy = pred[:, 1:] - pred[:, :-1]
		D_dx = pred[:, :, 1:] - pred[:, :, :-1]
		return D_dx, D_dy

	if type(pred_map) not in [tuple, list]:
		pred_map = [pred_map]

	loss = 0
	weight = 1.

	for scaled_map in pred_map:
		dx, dy = gradient(scaled_map)
		dx2, dxdy = gradient(dx)
		dydx, dy2 = gradient(dy)
		loss += (dx2.abs().mean() + dxdy.abs().mean() + dydx.abs().mean() + dy2.abs().mean())*weight
		weight /= 2.3  # don't ask me why it works better
	return loss